
$(document).on('click', '.deletePage', function(e) {
    e.preventDefault();
    console.log(e.currentTarget.value)
    var data = {
      '_id':e.currentTarget.value
    }
    $.ajax({
      type : 'DELETE',
      url : '/deletePage',
      data : JSON.stringify(data),
       beforeSend: function (xhr) {
           xhr.setRequestHeader('Content-Type', 'application/json');
           xhr.setRequestHeader('Accept', 'application/json');
       },
      success: function(result) {
        if (result) {
               $.toast({
                   heading: 'Success',
                   text: "Created",
                   showHideTransition: 'slide',
                   icon: 'success',
                   hideAfter: 1500
               });

               window.setTimeout(function(){
                window.location.reload();
                }, 2000);
           }
      },
      error: function(error) {
        $.toast({
            heading: 'Error',
            text: "Error found",
            showHideTransition: 'slide',
            icon: 'error',
            hideAfter: 1500
        });
        console.log(error);
      }
    })
  })
 