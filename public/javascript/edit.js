
$(document).on('click', '.saveDraft', function(e) {
  e.preventDefault();

  

  var data = {
    'type':$('#inputState option:selected').text(),
    'notificationContent':$('textarea#textarea1').val(),
    'menu':$('#inputState1 option:selected').text(),
    'content':$('#inputState2 option:selected').text(),
    'userDataField':$('#inputState3 option:selected').text(),
    'country':$('#inputState5 option:selected').text(),
    'state':$('#inputState6 option:selected').text(),
    'city':$('#inputState7 option:selected').text(),
    
  }
  $.ajax({
    type : 'PUT',
    url : '/editPage',
    data : JSON.stringify(data),
     beforeSend: function (xhr) {
         xhr.setRequestHeader('Content-Type', 'application/json');
         xhr.setRequestHeader('Accept', 'application/json');
     },
    success: function(result) {
      console.log(result);
      if (result) {
             $.toast({
                 heading: 'Success',
                 text: "Created",
                 showHideTransition: 'slide',
                 icon: 'success',
                 hideAfter: 1500
             });

             window.setTimeout(function(){
              window.location.reload();;
              }, 2000);
         }
    },
    error: function(error) {
      $.toast({
          heading: 'Error',
          text: "Error found",
          showHideTransition: 'slide',
          icon: 'error',
          hideAfter: 1500
      });
    }
  })
})

