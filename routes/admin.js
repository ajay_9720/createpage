const express = require('express');
const router = express.Router();

const auth = require('../middleware/auth');
const createEditPageController = require('../controllers/createEditPage');
const userController = require('../controllers/user');

router.get('/signup',function(req,res,next){
    res.render('user');
});
router.post('/signup',userController.register);
router.get('/login',auth.isLogin,function(req,res,next){
    res.render('login');
});
router.post('/login',userController.login);
router.get('/logout',auth.checkauthToken,userController.logout);

router.post('/addPage',auth.checkauthToken,createEditPageController.add);
router.get('/addPage',function(req,res,next){
    res.render('demo');
})
router.put('/editPage',createEditPageController.edit);
router.get('/editPage/:_id',auth.checkauthToken,function(req,res,next){
    res.render('edit');
})
router.delete('/deletePage',createEditPageController.delete);
router.get('/list',createEditPageController.get);

router.get('/statusChange',auth.checkauthToken,createEditPageController.status);

module.exports = router;