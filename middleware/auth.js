const User = require('../models/user');
const userService = require('../services/userServices');

exports.checkauthToken = (req,res,next) => {
    if(req.session.user.authToken == undefined){
        userService.sendResponse(res,401,'Error','Token is empty',err);
    }
    else{
        User.findOne({authToken:req.session.user.authToken})
        .then((result,err) => {
            if(result){
                global.authenticatedUser = result;
                next();
            }
            else{
                res.redirect('/login');
            }
        }).catch(err => {
            console.log('ERROR',err);
            res.send('Error');
        })
    }
}

exports.authenticateUser = function (req, res, next) {
    if (req.session.user) {
        next();
    } else {
        res.redirect('/login');
    }
};

exports.isLogin = function (req, res, next) {
    if (req.session.user) {
        res.redirect('/list');
    } else {
        next();
    }
};