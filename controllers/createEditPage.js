const express = require('express');
const admin = require('../models/createEdit');
const user = require('../models/user');

exports.add = (req,res,next) => {
    var obj = {
        type : req.body.type,
        notificationContent : req.body.notificationContent,
        contentLink: {
            menu : req.body.menu,
            content : req.body.content
        },
        targatedRecipients : {
            userDataField : req.body.userDataField
        },
        setGeoLocationArea : {
            country : req.body.country,
            state : req.body.state,
            city : req.body.city,
        },
        sentBy : authenticatedUser._id,
        status : 'Draft'
    }
    admin(obj).save()
    .then(result => {
        res.send(result);
    }).catch(err => {
        res.send('Error');
    })
}

exports.edit = (req,res,next) => {
    var obj = {
        type : req.body.type,
        notificationContent : req.body.notificationContent,
        contentLink: {
            menu : req.body.menu,
            content : req.body.content
        },
        targatedRecipients : {
            userDataField : req.body.userDataField
        },
        setGeoLocationArea : {
            country : req.body.country,
            state : req.body.state,
            city : req.body.city,
        },
        sentBy : authenticatedUser._id
    }
    admin.findOneAndUpdate({_id:req.body.id},{$set : obj})
    .then(result => {
        res.send(result);
    }).catch(err => {
        res.send('Error');
    })

}

exports.delete = (req,res,next) => {
    admin.findByIdAndRemove({_id:req.body._id})
    .then(result => {
        res.send(result);
    }).catch(err => {
        res.send('Error');
    })
}

exports.get = (req,res,next) => {
    admin.find({}).populate('sentBy')
    .then(result => {
        var obj = {
            admins : result
        }
        res.render('list',obj);
    }).catch(err => {
        res.send('Error');
    })
}

exports.status = (req,res,next) => {
    admin.findOneAndUpdate({sentBy : authenticatedUser._id, status: 'Draft'}, {$set : {status : 'Send'} })
    .then(result => {
        res.redirect('/list');
    }).catch(err => {
        res.send('Error');
    })
}