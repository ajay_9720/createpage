const user = require('../models/user');
const emailRegex = require('email-regex');
const userService = require('../services/userServices');
const bcrypt = require('bcrypt');

exports.register = (req, res, next) =>{
    userService.register(req.body)
        .then((user) => {
            res.redirect('/login');
        })
        .catch(err => {
            res.send(err);
        })
}


exports.login = (req, res, next) =>{
    userService.login(req.body)
        .then((user) => {
            req.session.user = user.data;
            res.redirect('/list');
            })
        .catch(err => {
            res.send(err);
        })
}

exports.logout = (req, res, next) =>{
    userService.logout()
        .then((user) => {
            req.session.destroy();
            res.redirect('/login')
        })
        .catch(err => {
            res.send(err);
        })
}

