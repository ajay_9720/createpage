const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const expressHbs = require('express-handlebars');
const path = require('path');
const session = require('express-session');
var helpers = require('handlebars-helpers')();

const app = express();
const adminRoutes = require('./routes/admin');

mongoose.connect('mongodb://localhost/page', { useNewUrlParser: true })
.then(() => {
console.log(`Succesfully Connected to the Mongodb Database at URL : mongodb://localhost/page`);
})
.catch(() => {
console.log(`Error Connecting to the Mongodb Database at URL : mongodb://localhost/page`);
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended : true
}))

app.engine('hbs',expressHbs({
    extname : '.hbs',
    helpers : helpers,
}
));
app.set('view engine','hbs');
app.set('views', path.join(__dirname, 'views'));

app.use(express.static(path.join(__dirname, 'public')))

app.use(session({
    secret: '53fYuRzabXfpvoJPheHJYuzZMDXOb05G',
    resave: false,
    saveUninitialized: true,
    cookie: {
        path: '/',
        httpOnly: false,
        secure: false,
        maxAge: 31536000000
    }
}));

app.use(adminRoutes);

app.listen(3000,function(req,res){
    console.log('App is runnning on port 3000');
})