const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const moment = require('moment');

const adminSchema = {

    type : String,

    notificationContent : {
        type : String,
    },

    contentLink : {
        menu : String,
        content : String
    },

    targatedRecipients : {
        userDataField : String,
    },

    setGeoLocationArea : {
        country : String,
        state : String,
        city : String
    },

    status : {
        type: String
    },

    sentBy : {
        type : Schema.Types.ObjectId,ref: 'user'
    },

    createdDate : {
        type : String,
        default : moment().format('LL')
    }

}

module.exports = mongoose.model('admin',adminSchema);
