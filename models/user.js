const mongoose = require('mongoose');
const userSchema = {

    name : {
        type : String,
    },

    email : {
        type : String,
        required : true
    },

    password : {
        type : String,
        required : true
    },

    authToken : String,
}

module.exports = mongoose.model('user',userSchema);