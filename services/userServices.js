const User = require('../models/user');
const bcrypt = require('bcrypt');
const emailRegex = require('email-regex');
const jwt = require('jsonwebtoken');


exports.register = (data) => {

    let checkUser = () => {
        return new Promise(function (resolve, reject) {
            User.findOne({ email: data.email })
                .then(result => {
                    if (result) {
                        var obj = {
                            msg: 'This User is Already Registered',
                            statusCode: 401,
                            data: []
                        }
                        reject(obj);

                    } 
                    else if(!data.password){
                        var obj1 = {
                            msg: 'Please Set Password',
                            statusCode: 401,
                            data: []
                        }
                        reject(obj1);
                    }
                    else if (data.password.length < 5) {
                        var obj2 = {
                            msg: 'Password Length is less than 5',
                            statusCode: 401,
                            data: []
                        }
                        reject(obj2);
                    } 
                    else {
                       var valid = emailRegex({exact: true}).test(data.email);
                        if(!valid){
                            var obj3 = {
                                msg: 'Please Type proper email',
                                statusCode: 401,
                                data: []
                            }
                            reject(obj3);
                        }else{
                        resolve(data);
                    } 
                }
        })
    })
    }

    let hashPassword = (retrieveobj) => {
        return new Promise(function (resolve, reject) {
            const user = new User({
                name: retrieveobj.name,
                email: retrieveobj.email,
            })
            bcrypt.hash(retrieveobj.password, 10).then(data => {
                user.password = data;
                user.save()
                    .then(userData => {
                        var obj1 = {
                            msg: 'SignUp Success',
                            statusCode: 200,
                            data: userData
                        }
                        resolve(obj1);
                        
                    }).catch(error1 => { reject(error1) });
            }).catch(error1 => { reject(error1) });
        })
    }


    return new Promise(function (resolve, reject) {
        checkUser(data)
            .then(hashPassword)
            .then(result1 => {
                var obj2 = {
                    msg: 'login success',
                    statusCode: 200,
                    data: result1
                }
                resolve(obj2);
            }).catch(error2 => { reject(error2) });
    })
}


exports.login = (data) => {
    let checkEmail = () => {
        return new Promise(function (resolve, reject) {
            User.findOne({ email: data.email })
                .then(result => {
                    if (!result) {
                        var obj = {
                            msg: 'User not Found',
                            statusCode: 401,
                            data: []
                        }
                        reject(obj);
                    }
                    else if (!data.email) {
                        var obj1 = {
                            msg: 'Please Set Email',
                            statusCode: 401,
                            data: []
                        }
                        reject(obj1);
                    }
                    else if (!data.password) {
                        var obj2 = {
                            msg: 'Please Set Password',
                            statusCode: 401,
                            data: []
                        }
                        reject(obj2);
                    }
                    else {
                        var obj3 = {
                            data: data,
                            result: result
                        }
                        resolve(obj3);
                    }
                })
        })
    }


    let comparePassword = (retrieveobj3) => {
        return new Promise(function (resolve, reject) {
            bcrypt.compare(retrieveobj3.data.password, retrieveobj3.result.password)
                .then(doMatch => {
                    if (doMatch) {
                        resolve(retrieveobj3);
                    } else {
                        var obj4 = {
                            msg: 'Password is Incorrect,Please Enter correct Password',
                            statusCode: 401,
                            data: []
                        }
                        reject(obj4);
                    }
                })
        })
    }

    let savegeneratedToken = (retrieveobj2) => {
        return new Promise(function (resolve, reject) {
            var accessToken = generatewebToken(retrieveobj2.result._id);
            User.findByIdAndUpdate(retrieveobj2.result.id, { $set: { authToken: accessToken } }, {new:true})
                .select('-password')
                .then(result1 => {
                    resolve(result1);
                }).catch(error3 => { reject(error3) });
        })
    }
    
    return new Promise(function (resolve, reject) {
        checkEmail(data)
            .then(comparePassword)
            .then(savegeneratedToken)
            .then(result => {
                var obj2 = {
                    msg: 'login success',
                    statusCode: 200,
                    data: result
                }
                resolve(obj2);
            })
            .catch(error4 => {
                reject(error4)
            });
    })
}

exports.logout = () => {
    return new Promise(function (resolve, reject) {
            console.log('fffffffffffff',authenticatedUser._id);
                User.findByIdAndUpdate(authenticatedUser._id, { $set: { authToken: '' } })
            .then(result3 => {
               resolve(result3);
        
        
    }).catch(error5 => { 
        reject(error5) });
})
}


exports.sendResponse = function (res, statusCode, status, message, data = '') {
    res.writeHead(statusCode, { 'Content-Type': 'application/json' });
    res.write(JSON.stringify({
        "status": status,
        "statusCode": statusCode,
        "message": message,
        "data": data
    }));
    res.end();
}


generatewebToken = (id) => {
    let claims = {
        iat : Date.now(),
        expiresIn : '1h',
        sub : 'authToken',
        data : id
    }
    const token = jwt.sign(
        claims,'jwtPrivateKey'
    );
    return token;
}
